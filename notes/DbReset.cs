﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieDB
{
    internal class DbReset
    {
        private string _path;

        public string Path
        {
            get { return _path; }
            set { _path = value; }
        }

        internal DbReset ()
        {
            _path = Directory.GetCurrentDirectory();
        }
    }
}
