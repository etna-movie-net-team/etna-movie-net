﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Resetter
{
    internal class Reset
    {
        private string _path;

        internal string Path
        {
            get { return _path; }
            set { _path = value; }
        }

        internal Reset()
        {
            _path = Directory.GetCurrentDirectory();
        }
    }
}
